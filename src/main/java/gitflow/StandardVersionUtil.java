package gitflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author 黎富强
 * @title: standard-version工具类
 * @projectName gitflow4idea
 * @description: 实现调用standard-verion命令完成自动生成changelog功能
 * @date 2020/4/1611:44
 */
public class StandardVersionUtil {
    /**
     * 生成changelog命令
     */
    private static final String MAKE_CHANGELOG_CMD = " npm run release ";



    /**
     * 实现生成并commit changgelog
     */
    public static Boolean doStandardVersion(String projectPath,String releaseName) throws Exception{
        boolean isSucess = false;
        String cmd = "cmd /c  "+MAKE_CHANGELOG_CMD;
        Process makeProcess  = Runtime.getRuntime().exec(cmd,null,new File(projectPath));
        makeProcess.waitFor();//阻塞线程，等待数据返回
        //取得命令结果的输出流
        InputStream fis=makeProcess.getInputStream();
        //用一个读输出流类去读
        InputStreamReader isr=new InputStreamReader(fis);
        //用缓冲器读行
        BufferedReader br=new BufferedReader(isr);
        String line=null;
        //直到读完为止
        while((line=br.readLine())!=null)
        {
            System.out.println(line);
        }
        int i = makeProcess.exitValue();//true is 0 ,false is 1
        if(i==0){
            isSucess=true;
        }
        return isSucess;
    }
    public static void main(String[] args ) throws Exception{
//        System.out.println(System.getProperty("user.dir"));
//        doStandardVersion("1.0.21");
    }
}
