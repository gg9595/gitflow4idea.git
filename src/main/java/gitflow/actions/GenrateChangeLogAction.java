package gitflow.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.Messages;
import git4idea.commands.GitCommandResult;
import git4idea.repo.GitRepository;
import git4idea.validators.GitNewBranchNameValidator;
import gitflow.StandardVersionUtil;
import gitflow.ui.NotifyUtil;
import org.jetbrains.annotations.NotNull;

/**
 * @author lps
 * @title: GenrateChangeLog
 * @projectName gitflow4idea
 * @description:   生成changelog
 * @date 2020/4/2120:03
 */
public class GenrateChangeLogAction extends AbstractStartAction {
    GenrateChangeLogAction() {
        super("Generate CHANGELOG");
    }
    GenrateChangeLogAction(GitRepository repo) {
        super(repo,"Generate CHANGELOG");
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        super.actionPerformed(e);

        new Task.Backgroundable(myProject,"Generate changelog",false){
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                if(!myRepo.getCurrentBranchName().equals("master")){
                    NotifyUtil.notifyError(myProject, "Error", "Please checkout master branch to generate changelog");
                }else {
                    //执行生成changelog
                    try {
                        if (!StandardVersionUtil.doStandardVersion(myProject.getBasePath(), null)) {
                            NotifyUtil.notifyError(myProject, "Error", "generate changelog failed");
                        } else {
                            NotifyUtil.notifySuccess(myProject, "generate changelog ", "generate changelog success");
                        }
                    } catch (Exception e) {
                        NotifyUtil.notifyError(myProject, "Error", "generate changelog failed" + e.getMessage());
                    }
                }


            }
        }.queue();

    }
}
